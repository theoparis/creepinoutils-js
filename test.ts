const { math } = require("./index");
const { Vector3 } = math.vector;

var vec1 = new Vector3();
console.log("Original: " + vec1);

var vec2 = vec1.addScalar(0, 1, 0);
console.log("add: " + vec2);