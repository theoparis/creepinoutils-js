export default {
    vector: require("./vector"),
    utils: require("./MathUtils"),
    quaternion: require("./quaternion"),

};